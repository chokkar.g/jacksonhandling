package com.msys.response.msys;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msys.response.msys.model.Arrays;

public class JacksonTest {

	public static void main(String[] args) {

		ObjectMapper objectMapper = new ObjectMapper();
		
		//https://github.com/FasterXML/jackson-databind/wiki/Serialization-Features
		// handling empty string instead of array
		objectMapper.configure(DeserializationFeature. ACCEPT_EMPTY_STRING_AS_NULL_OBJECT , true);
		
		//handlong list comes as object instead of array 
		objectMapper.enable(DeserializationFeature. ACCEPT_SINGLE_VALUE_AS_ARRAY);

		// Normal json
		File file = new File(
				"D:\\Documents\\eclipse-tintri\\com.msys.response\\src\\main\\java\\com\\msys\\response\\msys\\normal_response.json");

		// Pool list comes as object instead of array 
		File file1 = new File(
				"D:\\Documents\\eclipse-tintri\\com.msys.response\\src\\main\\java\\com\\msys\\response\\msys\\normal_response1.json");

		//Pool comes as empty string instead of array
		File file2 = new File(
				"D:\\Documents\\eclipse-tintri\\com.msys.response\\src\\main\\java\\com\\msys\\response\\msys\\normal_response2.json");

		try {
			Arrays arrays = objectMapper.readValue(file2, Arrays.class);
			System.out.println("Array Response" + arrays);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
