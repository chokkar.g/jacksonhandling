package com.msys.response.msys.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Arrays {
	
	private Array array;

	 @JsonProperty("array")
	public Array getArray() {
		return array;
	}

	 @JsonProperty("array")
	public void setArray(Array array) {
		this.array = array;
	}

	@Override
	public String toString() {
		return "Arrays [array=" + array + "]";
	}
	 
	 
}
