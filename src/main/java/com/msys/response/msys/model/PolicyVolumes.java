package com.msys.response.msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class PolicyVolumes {
    private List<FluffyVolume> volume;

    @JsonProperty("volume")
    public List<FluffyVolume> getVolume() { return volume; }
    @JsonProperty("volume")
    public void setVolume(List<FluffyVolume> value) { this.volume = value; }
}
