package com.msys.response.msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Welcome {
    private Array array;

    @JsonProperty("array")
    public Array getArray() { return array; }
    @JsonProperty("array")
    public void setArray(Array value) { this.array = value; }
}
